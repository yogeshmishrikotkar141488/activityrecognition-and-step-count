package com.yogi.sample.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.yogi.sample.WalkingApplication;
import com.yogi.sample.activitys.SenserDemoActivity;
import com.yogi.sample.demo.R;
import com.yogi.sample.tools.Debug;

import java.util.List;

/**
 * Created by yogesh.mishrikotkar on 11-10-2016.
 */

public class ActivityRecognizedService extends IntentService {

    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    /**
     * Called when a new activity detection update is available.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // If the intent contains an update
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            DetectedActivity mostProbableActivity = result.getMostProbableActivity();
            // Get the confidence percentage for the most probable activity
            int confidence = mostProbableActivity.getConfidence();
            int activityType = mostProbableActivity.getType();
            mostProbableActivity.getVersionCode();
            if (confidence >= 50) {
                //String mode = getNameFromType(activityType);
                if (activityType == DetectedActivity.ON_FOOT) {
                    DetectedActivity betterActivity = walkingOrRunning(result.getProbableActivities());
                    if (null != betterActivity)
                    {
                        sendNotification( betterActivity);
                    }
                }else if (activityType == DetectedActivity.STILL) {
                    DetectedActivity betterActivity = stillActivity(result.getProbableActivities());
                    if (null != betterActivity)
                    {
                        sendNotification( betterActivity);
                    }
                }
            }
        }
    }

    private void sendNotification(  DetectedActivity activity) {
        switch( activity.getType() ) {
            case DetectedActivity.IN_VEHICLE: {
                LocalBroadcastManager.getInstance(WalkingApplication.getContext()).sendBroadcast(new Intent(SenserDemoActivity.ACTION_ACTIVITY_IN_VEHICLE));
                Debug.print( "ActivityRecogition", "In Vehicle: " + activity.getConfidence() );
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                LocalBroadcastManager.getInstance(WalkingApplication.getContext()).sendBroadcast(new Intent(SenserDemoActivity.ACTION_ACTIVITY_ON_BICYCLE));

                Debug.print( "ActivityRecogition", "On Bicycle: " + activity.getConfidence() );
                break;
            }
            case DetectedActivity.ON_FOOT: {
                LocalBroadcastManager.getInstance(WalkingApplication.getContext()).sendBroadcast(new Intent(SenserDemoActivity.ACTION_ACTIVITY_ON_FOOT));
                Debug.print( "ActivityRecogition", "On Foot: " + activity.getConfidence() );
                break;
            }
            case DetectedActivity.RUNNING: {
                LocalBroadcastManager.getInstance(WalkingApplication.getContext()).sendBroadcast(new Intent(SenserDemoActivity.ACTION_ACTIVITY_ON_RUNNING));
                Debug.print( "ActivityRecogition", "Running: " + activity.getConfidence() );
                break;
            }
            case DetectedActivity.STILL: {
                LocalBroadcastManager.getInstance(WalkingApplication.getContext()).sendBroadcast(new Intent(SenserDemoActivity.ACTION_ACTIVITY_ON_STILL));
                Debug.print( "ActivityRecogition", "Still: " + activity.getConfidence() );
                break;
            }
            case DetectedActivity.TILTING: {
                Debug.print( "ActivityRecogition", "Tilting: " + activity.getConfidence() );
                break;
            }
            case DetectedActivity.WALKING: {
                LocalBroadcastManager.getInstance(WalkingApplication.getContext()).sendBroadcast(new Intent(SenserDemoActivity.ACTION_ACTIVITY_ON_WALKING));
                Debug.print( "ActivityRecogition", "Walking: " + activity.getConfidence() );

                break;
            }
            case DetectedActivity.UNKNOWN: {
                Debug.print( "ActivityRecogition", "Unknown: " + activity.getConfidence() );
                break;
            }
        }
    }


    private DetectedActivity walkingOrRunning(List<DetectedActivity> probableActivities) {
        DetectedActivity myActivity = null;
        int confidence = 0;
        for (DetectedActivity activity : probableActivities) {
            if (activity.getType() != DetectedActivity.RUNNING && activity.getType() != DetectedActivity.WALKING)
                continue;

            if (activity.getConfidence() >  confidence) {
                confidence = activity.getConfidence();
                myActivity = activity;
            }
        }

        return myActivity;
    }

    private DetectedActivity stillActivity(List<DetectedActivity> probableActivities) {
        DetectedActivity myActivity = null;
        int confidence = 0;
        for (DetectedActivity activity : probableActivities) {
            if (activity.getType() != DetectedActivity.STILL)
                continue;

            if (activity.getConfidence() > confidence)
                myActivity = activity;
        }

        return myActivity;
    }







}