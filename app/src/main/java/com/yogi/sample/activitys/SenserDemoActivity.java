package com.yogi.sample.activitys;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.yogi.sample.WalkingApplication;
import com.yogi.sample.demo.R;
import com.yogi.sample.services.ActivityRecognizedService;
import com.yogi.sample.services.StepSensorListener;
import com.yogi.sample.tools.Debug;

public class SenserDemoActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    StepSensorListener stepCountDetector;
    private TextView stepsCountTextView;

    public int stepCount;
    private GoogleApiClient mApiClient;

    ProgressDialog progressDialog;
    private TextView activityFindingView;

    public static final String ACTION_ACTIVITY_IN_VEHICLE = "ACTION_ACTIVITY_IN_VEHICLE";
    public static final String ACTION_ACTIVITY_ON_BICYCLE = "ACTION_ACTIVITY_ON_BICYCLE";
    public static final String ACTION_ACTIVITY_ON_FOOT = "ACTION_ACTIVITY_ON_FOOT";
    public static final String ACTION_ACTIVITY_ON_RUNNING = "ACTION_ACTIVITY_ON_RUNNING";
    public static final String ACTION_ACTIVITY_ON_STILL = "ACTION_ACTIVITY_ON_STILL";
    public static final String ACTION_ACTIVITY_ON_WALKING = "ACTION_ACTIVITY_ON_WALKING";

 boolean isChecked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_fit_main);

        activityFindingView = (TextView) findViewById(R.id.activity_finding_view);

        init();

        stepCountDetector = new StepSensorListener(new StepSensorListener.StepDetector() {
            @Override
            public void onStep() {
                stepCount =stepCount+1;
                stepsCountTextView.setText(" Steps count is " + stepCount);
            }
        });

        CheckBox checkBox = (CheckBox) findViewById(R.id.isRingCheckBox);
        isChecked = checkBox.isChecked();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean  checked) {
                isChecked = checked;
            }
        });

    }

    private void init() {
        progressDialog = new ProgressDialog(this);
        stepCount = 0;
        stepsCountTextView = (TextView) findViewById(R.id.sample_logview);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        stepsCountTextView.setText("Steps count is " + 0);


        progressDialog.setMessage("Please wait");
        progressDialog.show();


        registerBrodcast();
        beginConnectGoogleClient();
    }

    private void beginConnectGoogleClient() {
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mApiClient.connect();
    }

    private void registerBrodcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_ACTIVITY_ON_FOOT);
        intentFilter.addAction(ACTION_ACTIVITY_ON_RUNNING);
        intentFilter.addAction(ACTION_ACTIVITY_ON_STILL);
        intentFilter.addAction(ACTION_ACTIVITY_ON_WALKING);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,intentFilter);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String activityAction ="";
            if (intent != null && intent.getAction() != null) {
                String action = intent.getAction();
                if (action.equalsIgnoreCase(ACTION_ACTIVITY_ON_FOOT)) {
                    stepCountDetector.setSensitivity(10);
                    activityAction = "just standing";
                } else if (action.equalsIgnoreCase(ACTION_ACTIVITY_ON_RUNNING)) {
                    stepCountDetector.setSensitivity(20);
                    activityAction = "running";
                } else if (action.equalsIgnoreCase(ACTION_ACTIVITY_ON_STILL)) {
                    stepCountDetector.setSensitivity(10);
                    fireNotification(context, "standing at place ");
                    activityAction="standing at place";
                } else if (action.equalsIgnoreCase(ACTION_ACTIVITY_ON_WALKING)) {
                    stepCountDetector.setSensitivity(15);
                    activityAction ="walking";
                }
                if(!activityAction.isEmpty()){
                    activityFindingView.setText(activityAction);
                    fireNotification(context,activityAction);
                }

            }
        }
    };

    private void startSensor() {
        mSensorManager.registerListener(stepCountDetector, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    protected void onDestroy() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        if(mSensorManager!=null){
            mSensorManager.unregisterListener(stepCountDetector);
        }
        stepCountDetector = null;
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(mApiClient!=null){
            Intent intent = new Intent( this, ActivityRecognizedService.class );
            PendingIntent pendingIntent = PendingIntent.getService( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );
            ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates( mApiClient, 0, pendingIntent );
        }else{
            Debug.displayToast(this,"unable to start Activity Recognition");
        }
        startSensor();
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void fireNotification(Context context, String activityValue){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(context.getResources().getString(R.string.app_name));
        mBuilder.setSmallIcon(R.drawable.ic_launcher);

        if(isChecked){
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(alarmSound);

        }
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
        final float scale = context.getResources().getDisplayMetrics().density;
        int p = (int) (64 * scale + 0.5f);
        mBuilder.setLargeIcon(Bitmap.createScaledBitmap(bm, p, p, true));
        Intent resultIntent = new Intent();
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context,
                100, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();

        inboxStyle.setBigContentTitle(context.getResources().getString(R.string.app_name));
        mBuilder.setWhen(System.currentTimeMillis());


        mBuilder.setContentTitle(context.getResources().getString(R.string.app_name));
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(getResources().getString(R.string.app_name));
        mBuilder.setStyle(bigTextStyle);
        mBuilder.setContentText("Hey are you " + activityValue + "? \n Steps count " + stepCount);

        NotificationManager mNotificationManager = (NotificationManager) WalkingApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        Notification noti = mBuilder.build();
        /* notificationID allows you to update the notification later on. */
        mNotificationManager.notify(100, noti);


    }
}
