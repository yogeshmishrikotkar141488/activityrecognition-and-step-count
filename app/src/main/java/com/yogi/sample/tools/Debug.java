package com.yogi.sample.tools;

import android.content.Context;
import android.widget.Toast;


import com.yogi.sample.demo.R;

/**
 * Created by yogesh.mishrikotkar on 11-10-2016.
 */

public class Debug {

    public static void print(String msg){
        print("Yogesh Your log ",msg);
    }
    public static void print(Exception msg){
        print("Yogesh Your log ",""+msg);
    }

    public static void print(String tag, String msg){

        System.out.println(tag+"       "+msg);
    }

    public static void displayToast(Context context, String msg){
        Toast.makeText(context, msg,Toast.LENGTH_SHORT).show();
    }
}
