package com.yogi.sample;

import android.app.Application;
import android.content.Context;

/**
 * Created by yogesh.mishrikotkar on 11-10-2016.
 */

public class WalkingApplication extends Application {

   private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context= this;
    }

    public static Context getContext(){
        return context;
    }
}
